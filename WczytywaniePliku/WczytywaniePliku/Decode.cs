﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WczytywaniePliku
{
    class Decode
    {
        private int dlugosc, precyzja, wysokosc, szerokosc;


        public void Dekoduj_Wlasnosci(byte[] Obrazek)
        {
            for (int i = 0; i < Obrazek.Length; i++)
            {
                if (Obrazek[i] == 0xff && Obrazek[i + 1] == 0xc0)
                {
                    Console.WriteLine("Wykryto Znacznik S0F0");
                    dlugosc = (Obrazek[i + 2]) * 256 + Obrazek[i + 3];
                    precyzja = (Obrazek[i + 4]);
                    wysokosc = (Obrazek[i + 5] * 256 + Obrazek[i + 6]);
                    szerokosc = (Obrazek[i + 7] * 256 + Obrazek[i + 8]);
                }
                else if (Obrazek[i] == 0xff && Obrazek[i + 1] == 0xd8)
                {
                    Console.WriteLine("Wykryto Plik jpg, rozpoczynam dekodowanie");
                }
            }
        }
        public void Wyswietl_Wlasnosci()
        {
            Console.WriteLine("Dlugosc: "+dlugosc);
            Console.WriteLine("Precyzja: "+precyzja);
            Console.WriteLine("Wysokosc: "+wysokosc);
            Console.WriteLine("Szerokosc: "+szerokosc);
        }


    }
}
